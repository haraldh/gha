FROM registry.gitlab.com/enarx/dev:latest

COPY start.sh /root/
CMD /root/start.sh
